import Nav from './Nav';

function AttendeesList(props) {
    <div className="table-responsive">
        <table className="table table-striped">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Conference</th>
            </tr>
            </thead>
            <tbody>
            {/* for (let attendee of props.attendees) {
            <tr>
            <td>{ attendee.name }</td>
            <td>{ attendee.conference }</td>
            </tr>} */}
    {props.attendees.map(attendee => {
    return (
        <tr key={attendee.href}>
        <td>{ attendee.name }</td>
        <td>{ attendee.conference }</td>
        </tr>
    );
    })}
            </tbody>
        </table>
        </div>
    };

export default AttendeesList;
