function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card shadow mb-5">
        <img src=${pictureUrl} class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${startDate} - ${endDate}</div>
      </div>
    `;
  }

function createPlaceHolder(){
  return `<div class="card" aria-hidden="true">
  <img src="..." class="card-img-top">
  <div class="card-body">
    <h5 class="card-title placeholder-glow"><span class="placeholder col-6"></span></h5>
    <h6 class="card-subtitle mb-2 text-muted placeholder-glow"><span class="placeholder col-3"></span></h6>
    <p class="card-text placeholder-glow"><span class="placeholder col-7"></span>
    <span class="placeholder col-4"></span>
    <span class="placeholder col-4"></span>
    <span class="placeholder col-6"></span>
    <span class="placeholder col-8"></span></p>
  </div>
  <div class="card-footer placeholder-glow"><span class="placeholder col-2"></span></div>
</div>
`;
}

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        throw new Error("Could not successfully gather conference data")
      } else {
        // let colVar = 1;
        const data = await response.json();
        const columns = document.querySelectorAll('.col');
        console.log(columns);
        let columnIndex = 0
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const origStartDate = new Date(details.conference.starts.slice(0,10));
            console.log(origStartDate);

            const startDate = `${origStartDate.getMonth()+1}/${origStartDate.getDate()}/${origStartDate.getFullYear()}`
            console.log(startDate)

            const origEndDate = new Date(details.conference.ends.slice(0,10));
            console.log(origEndDate);

            const endDate = `${origEndDate.getMonth()+1}/${origEndDate.getDate()}/${origEndDate.getFullYear()}`
            console.log(endDate)

            const location = details.conference.location.name;

            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            // const column = document.querySelector('.col');
            // column.innerHTML += html;

            const column = columns[columnIndex];
            console.log(column);
            // const column = document.querySelector('.col');
            column.innerHTML += html;
            // colVar++
            columnIndex = (columnIndex + 1) % columns.length;
            console.log(columnIndex);
          }
        }
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error("Error:", e);
    }

  });
