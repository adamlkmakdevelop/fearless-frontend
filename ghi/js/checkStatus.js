window.addEventListener('DOMContentLoaded', async () => {
// Get the cookie out of the cookie store
    const payloadCookie = await cookieStore.get({name: "jwt_access_payload"});
    if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
    const encodedPayload = JSON.parse(payloadCookie.value);

  // Convert the encoded payload from base64 to normal string
    const decodedPayload = atob(encodedPayload)

  // The payload is a JSON-formatted string, so parse it
    const payload = JSON.parse(decodedPayload)

  // Print the payload
    console.log(payload);

  // Check if "events.add_conference" is in the permissions.
        console.log(payload.user.perms)
        if(payload.user.perms.indexOf("events.add_conference") !== -1){
            let addConference = document.getElementById('nav-link-new-conference');

  // If it is, remove 'd-none' from the link
            addConference.classList.remove('d-none')};

  // Check if "events.add_location" is in the permissions.
        if(payload.user.perms.indexOf("events.add_location") !== -1){
            let addLocation = document.getElementById('nav-link-new-location');

  // If it is, remove 'd-none' from the link
            addLocation.classList.remove('d-none')};

  // Check if "presentations.add_presentation" is in the permissions.
        if(payload.user.perms.indexOf("presentations.add_presentation") !== -1){
            let add_presentation = document.getElementById('nav-link-new-presentation');
  // If it is, remove 'd-none' from the link
            add_presentation.classList.remove('d-none')}
}
});
