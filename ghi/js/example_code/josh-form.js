import React, { useState } from 'react';

const Form = function(props) {
    const [name, setName] = useState('')
    const [date, setDate] = useState('');
    const [email, setEmail] = useState('');
    const [text, setText] = useState('');
    const [selectedOption, setSelectedOption] = useState('');

    const handleNameChange = (event) => {
        console.log(event.target.value);
        // console.dir(event.target)
        setName(event.target.value);
    }
    const handleDateChange = (event) => {
        setDate(event.target.value);
    }
    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    }
    const handleTextChange = (event) => {
        setText(event.target.value);
    }
    const handleSelectedOptionChange = (event) => {
        setSelectedOption(event.target.value);
    }

    const resetFormState = () => {
        setName('');
        setDate('');
        setEmail('');
        setText('');
        setSelectedOption('');
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const body = {
            name,
            date,
            email,
            text,
            selectedOption
        }

        const response = await fetch('http://www.coolwebsite.com', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body)
        })

        if (response.ok) {
            resetFormState();
        }

    }

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <input
                    onChange={handleNameChange}
                    placeholder="name"
                    type="text"
                    value={name}
                />
            </div>
            <div>
                <input
                    onChange={handleDateChange}
                    placeholder="date"
                    type="date"
                    value={date}
                />
            </div>
            <div>
                <input
                    onChange={handleEmailChange}
                    placeholder="email"
                    type="email"
                    value={email}
                    />
            </div>
            <div>
                <textarea
                    placeholder="long text"
                    value={text}
                    onChange={handleTextChange}
                ></textarea>
            </div>
            <div>
                <select value={selectedOption} onChange={handleSelectedOptionChange}>
                    <option value="1">First option</option>
                    <option value="2">Second option</option>
                    <option value="3">Third option</option>
                </select>
            </div>
            <input type="submit"></input>
        </form>
    );
}

export default Form;
